import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        String myName;

        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name: ");
        String firstName = myObj.nextLine();
        System.out.println("Last Name: ");
        String lastName  = myObj.nextLine();

        myName = firstName + " " + lastName;

        System.out.println("First Subject Grade: ");
        double firstSubject = myObj.nextDouble();

        System.out.println("Second Subject Grade: ");
        double secondSubject= myObj.nextDouble();

        System.out.println("Third Subject Grade: ");
        double thirdSubject = myObj.nextDouble();

        double average = ((firstSubject + secondSubject + thirdSubject) / 3);

        System.out.println("Good day, " + myName + ".");
        System.out.println("Your grade average is: " + average);

    }
}